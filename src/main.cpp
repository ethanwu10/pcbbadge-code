#include <Arduino.h>
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>


ISR(PCINT0_vect)
{
  // This is called when the interrupt occurs, but I don't need to do anything in it
}

#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
// http://www.insidegadgets.com/wp-content/uploads/2011/02/ATtiny85_watchdog_example.zip
void system_sleep()
{
  cbi(ADCSRA, ADEN);                   // switch Analog to Digitalconverter OFF
  power_all_disable();                 // power off ADC, Timer 0 and 1, serial interface
  set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here
  noInterrupts();                      // timed sequence coming up
  sleep_enable();
  interrupts();       // interrupts are required now
  sleep_cpu();        // System sleeps here
  sleep_disable();    // System continues execution here when watchdog timed out
  power_all_enable(); // power everything back on
//  sbi(ADCSRA,ADEN);                    // switch Analog to Digitalconverter ON
}

// 0=16ms, 1=32ms, 2=64ms, 3=128ms, 4=250ms, 5=500ms, 6=1 sec,7=2 sec, 8=4 sec, 9= 8sec
void setup_watchdog(int ii)
{
  byte bb;
  //int ww;
  if (ii > 9)
    ii = 9;
  bb = ii & 7;
  if (ii > 7)
    bb |= (1 << 5);
  bb |= (1 << WDCE);
  //ww=bb;

  MCUSR &= ~(1 << WDRF);
  // start timed sequence
  WDTCR |= (1 << WDCE) | (1 << WDE);
  // set new watchdog timeout value
  WDTCR = bb;
  WDTCR |= _BV(WDIE);
}

// Watchdog Interrupt Service / is executed when watchdog timed out
ISR(WDT_vect)
{
  // wdt_disable();  // disable watchdog
}

// 6 LED on X1, X2, X3 vs Y1, Y2 matrix
constexpr int t = 32; //LED pulse on period in milliseconds
constexpr int n = 9;  //light on duty cycle % = (2/n+m+2)*100, i.e. 12.5%
constexpr int m = 5;  //light off between two blinks 32 * 5 = 160 ms

constexpr uint8_t ledMapping[] = {021, 022, 024, 011, 012, 014};

/**
 * Pulse an LED
 * 
 * @param ledN LED number, 1-6
 */
template<uint8_t ledN>
void ledPulse()
{
  PORTB = ledMapping[ledN - 1];
  delay(t);
  PORTB = 0;
}

void setup()
{
  // Setup pins for output
  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);
  pinMode(2, OUTPUT);
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  // Set all pins to LOW to turn off all LEDs
  digitalWrite(0, 0); //pin x1
  digitalWrite(1, 0); //pin x2
  digitalWrite(2, 0); //pin x3
  digitalWrite(3, 0); //pin y1
  digitalWrite(4, 0); //pin y2

  // pulse all LEDs just for show
  ledPulse<1>();
  ledPulse<2>();
  ledPulse<3>();
  ledPulse<4>();
  ledPulse<5>();
  ledPulse<6>();

  // WDTO_15MS, WDTO_30MS, WDTO_60MS, WDTO_120MS, WDTO_250MS, WDTO_500MS,
  // WDTO_1S, WDTO_2S, WDTO_4S, WDTO_8S
  setup_watchdog(WDTO_30MS); // Periodic Heartbeat to awaken deep sleep()
  sleep_disable();
}

void sleepXtimes(int cycleNumber)
{
  for (int i = 0; i < cycleNumber; i++)
  {
    system_sleep();
  }
}

void loop()
{
  wdt_reset(); 

  // pulse LED twice (sleep m times in between), then sleep n times
  ledPulse<1>();
  sleepXtimes(m);
  ledPulse<1>();
  sleepXtimes(n);

  ledPulse<2>();
  sleepXtimes(m);
  ledPulse<2>();
  sleepXtimes(n);

  ledPulse<3>();
  sleepXtimes(m);
  ledPulse<3>();
  sleepXtimes(n);

  ledPulse<4>();
  sleepXtimes(m);
  ledPulse<4>();
  sleepXtimes(n);

  ledPulse<5>();
  sleepXtimes(m);
  ledPulse<5>();
  sleepXtimes(n);

  ledPulse<6>();
  sleepXtimes(m);
  ledPulse<6>();
  sleepXtimes(n);

}
