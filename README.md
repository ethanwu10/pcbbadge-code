PCB Badge Code
==============

Code to run on the [PCB Badge][badge].

To upload this code yourself, build an [Arduino ISP][ardisp] - all you need
is an Arduino, 6 wires (with a male end), and a capacitor.

You will also likely need to specify the COM port (or device, if you are on
Mac/Linux) of your Arduino programmer in `platformio.ini` by adding an
`upload_port` key at the bottom of the file.

This is a [PlatformIO][pio] project, however since the code has no dependencies other
than the Arduino framework, you can use the Arduino IDE with [ATTiny board
support][ard-att] if you wish.

[badge]: https://gitlab.com/ethanwu10/pcbbadge
[ardisp]: http://highlowtech.org/?p=1706
[pio]: https://platformio.org/
[ard-att]: http://highlowtech.org/?p=1695
